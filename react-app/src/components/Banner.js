import { Button, Row, Col } from 'react-bootstrap';

export default function Banner({page}) {
	if(page == "home"){
		return(
			<Row>
				<Col>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere!</p>
					<Button variant="primary"> Enroll Now! </Button>
				</Col>
			</Row>
		)
	} else{
		return(
			<>
			<h1>Page Not Found</h1>
			Go back to <a href = "/">homepage</a>
			</>
		)
	}
};