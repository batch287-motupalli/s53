import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  
  // The React.StrictMode is a component tool for highlighting potential problems in an application and providing more information regarding the errors encountered.
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "John Smith";
// const element = <h1>Hello, {name}!</h1>;

//root.render(element);

// const user = {
//   firstName: 'Jane',
//   lastName: 'Smith'
// };

// function formatName(user) {
//   return user.firstName + " " + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>
// root.render(element);